<?php declare(strict_types=1);

namespace Jawira\AStar;

/**
 * @author Jawira Portugal <dev@tugal.be>
 */
abstract class AbstractNode
{
  /**
   * AbstractNode constructor.
   *
   * Unlike other languages, in PHP constructor is inherited by subclasses.
   * Therefore, you might not re-implement this.
   *
   * @param mixed                           $state  Current state
   * @param int|float                       $gCost  Cost from goal to current state
   * @param null|\Jawira\AStar\AbstractNode $parent Parent node
   */
  public function __construct(protected mixed $state, protected int|float $gCost, protected ?AbstractNode $parent = null)
  {
  }

  /**
   * f = g + h
   *
   * This is the addition of 'g'  and 'h'. 'g' cost is set using constructor,
   * while 'h' cost is returned by getHCost method.
   */
  public function getFCost(): int|float
  {
    return $this->gCost + $this->getHCost();
  }

  /**
   * Heuristic cost.
   */
  abstract protected function getHCost(): int|float;

  /**
   * Get all neighbors nodes.
   *
   * @return \Jawira\AStar\AbstractNode[]
   */
  abstract public function expand(): iterable;

  /**
   * Current node hash.
   *
   * This could be something simple, not necessarily an implementation of a
   * real hash algorithm. Nevertheless, it has to identify uniquely this node.
   */
  abstract public function hash(): string;
}

<?php declare(strict_types=1);

namespace Jawira\AStar;

use SplPriorityQueue;

/**
 * @author Jawira Portugal <dev@tugal.be>
 */
abstract class AbstractProblem
{
  /** Nodes to be visited. */
  protected SplPriorityQueue $openList;

  /** @var string[] List of hash of known nodes. */
  protected array $knownNodes = [];


  public function __construct(AbstractNode $initialNode)
  {
    $this->openList = new SplPriorityQueue();
    $this->openList->insert($initialNode, $initialNode->getFCost() * -1);
    array_unshift($this->knownNodes, $initialNode->hash());
  }

  /**
   * Returns _true_ if node parameter is the goal, _false_ otherwise.
   *
   * This will stop search.
   */
  abstract protected function isGoal(AbstractNode $node): bool;

  /**
   * Start searching process.
   *
   * Will return goal node, or null if there is no solution is found.
   */
  public function search(): ?AbstractNode
  {
    $goal = null;

    while ($this->openList->valid()) {

      $current = $this->openList->extract();
      assert($current instanceof AbstractNode);

      foreach ($current->expand() as $successor) {

        if ($this->isGoal($successor)) {
          // Stop since we have found a solution.
          $goal = $successor;
          break 2;
        }

        $hash = $successor->hash();
        if (in_array($hash, $this->knownNodes)) {
          // Node was already discovered.
          continue;
        }

        $this->openList->insert($successor, $successor->getFCost() * -1);
        array_unshift($this->knownNodes, $hash);
      }
    }

    return $goal;
  }
}
